//
// Created by kei666 on 18/11/13.
//

#ifndef PRACTICE_CUDA_VECDOUBLE_H
#define PRACTICE_CUDA_VECDOUBLE_H

void vecDouble(int *hIn, int *hOut, const int n);

#endif //PRACTICE_CUDA_VECDOUBLE_H
